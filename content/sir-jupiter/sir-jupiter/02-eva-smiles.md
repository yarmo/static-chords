+++
title = "Eva Smiles"
weight = 2

[extra]
number = "2"
+++

{title: Eva Smiles}
{artist: Sir Jupiter}
{album: Sir Jupiter}
{year: 2013}

{start_of_bridge: Intro}
[Fm7]
{end_of_bridge}

{start_of_verse: Verse 1}
[Fm7]Eva smiles, puts her bonnet on and leaves for a while.
[Bbm7]Eva smiles, puts her bonnet on and [Fm7]leaves for a while.
[Cm7]There's no simple [Bbm7]thing that I should have done.
{end_of_verse}

{start_of_bridge: Interlude}
[Db] [Eb] [Db] [Eb]
[Fm7]
{end_of_bridge}

{start_of_verse: Verse 2}
[Fm7]Yellow sky, she's not turning blue until we try.
[Bbm7]Yellow sky, she's not turning blue un[Fm7]til we try.
[Cm7]Tell me what you [Bbm7]would, what you would have done.
[Db] [Eb] [Db] [Eb]•
{end_of_verse}

{start_of_chorus: Chorus 1}
[Ab]Come on sun, just [Eb]shine for me.
[B]There's no right so [Db]you won't see
[Ab]All the trouble that I [Eb]find myself
[B]In to make sure that I [Db]live this moment with you.
{end_of_chorus}

{start_of_bridge: Instrumental}
[Fm7] [Bbm7] [Fm7] [Cm7]
[Db] [Eb] [Db] [Eb]
[Fm7]
{end_of_bridge}

{start_of_verse: Verse 2}
[Fm7]Look at you, Venus got you shining.
[Bbm7]Look at you, Venus got you shining. [Fm7]
[Cm7]What has never been [Bbm7]done before? [Fm7]
[Bbm7]Eva smiles, puts her bonnet on and [Fm7]leaves for a while.
[Cm7] [Bbm7]•
[Db] [Eb] [Db] [Eb]•
{end_of_verse}

{start_of_chorus: Chorus 2}
[Ab]Come on sun, just [Eb]shine for me.
[B]There's no right so [Db]you won't see
[Ab]All the trouble that I [Eb]find myself
[B]In to make sure that I [Db]live this moment with you.
{end_of_chorus}

{start_of_bridge: Outro}
[Fm7]
{end_of_bridge}
