+++
title = "Destination Unknown"
weight = 10

[extra]
number = "10"
+++

{title: Destination Unknown}
{artist: Sir Jupiter}
{album: Sir Jupiter}
{year: 2013}

{start_of_bridge: Intro}
[E] [A] [E]
{end_of_bridge}

{start_of_verse: Verse 1}
[E]Been driving around for [A]days.
[E]Won't stop, just wanna get a[A]way.
[E]Left my town, left my pride and my [A]joy,
[E]For some far destination un[A]known.
{end_of_verse}

{start_of_bridge: Interlude}
[E]
{end_of_bridge}

{start_of_chorus: Chorus 1}
[B]Some blues on a stereo track.
[B]In my rear view, everything's black.
[B]Drifting out on the motorway,
[B]Grimness trips into [D]day. [E]
{end_of_chorus}

{start_of_bridge: Bridge}
[E] [A]
{end_of_bridge}

{start_of_verse: Verse 2}
[E]Mary told me, said I had the right to [A]know,
[E]This commitment that she made long a[A]go.
[E]She felt sorry, so sorry she could drown in [A]shame.
[E]Yeah and Mary wasn't even her [A]name.
{end_of_verse}

{start_of_chorus: Chorus 2}
[B]Some blues on a stereo track.
[B]In my rear view, everything's black.
[B]Drifting out on the motorway,
[B]Grimness trips into [D]day. [E]
{end_of_chorus}

{start_of_bridge: Instrumental}
[E] [D] [A/C#] [B]
[D] [E] [A] [E/G#] [D/F#] [C#m/E] [B/F#]
{end_of_bridge}

{start_of_verse: Verse 3}
[E]And so, I'm now back on the [A]road,
[E]My destination yet to un[A]fold.
[E]Feel the rhythm of my Cadi[A]llac,
[E]As I'm never, no, never going [A]back.
{end_of_verse}

{start_of_chorus: Chorus 3}
[B]Some blues on a stereo track.
[B]In my rear view, everything's black.
[B]Drifting out on the motorway,
[B]Grimness trips into [D]day. [E]
{end_of_chorus}

{start_of_bridge: Outro}
[E] [A] [E]
[E] [D] [C#m] [B]
{end_of_bridge}
