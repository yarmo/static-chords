+++
title = "Wings EP"
weight = 2016
template = "album.html"
page_template = "album_song.html"
sort_by = "weight"

[extra]
year = "2016"
+++