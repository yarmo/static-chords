+++
title = "Angel On Earth"
weight = 5

[extra]
number = "5"
+++

{title: Angel On Earth}
{artist: Sir Jupiter}
{album: Wings EP}
{year: 2016}

{start_of_bridge: Intro}
The [A]clock is ticking and I [Bm]feel the moment slipping [A]through my hands. [Bm]
Soon the [A]movie is over, I'm too [Bm]cold and frozen to [G]understand. [Bm]
[Em]Don't you know what's [F#m]coming when they [G]switch on all the [A]lights?
There's an [Bm]angel on Earth, gonna hit rock bottom tonight.
There's an [Bm]angel on Earth, gonna [Em]hit rock bottom to[D]night. [A] [G]
We're gonna [A]hit rock bottom tonight.
{end_of_bridge}

{start_of_bridge: Interlude}
[D] [A] [G] [A]
{end_of_bridge}

{start_of_verse: Verse 1}
[G]It's like the world's spinning 'round on [D]itself,
And then all of a sudden, the ma[A]chines stop running,
And there's no one left to be [G]found.
And for a moment in time, I was [D]lost,
'Til the moonlight faded and [A]sunbeam raided
All the shadows on the ground. [G]
{end_of_verse}

{start_of_verse: Verse 2}
[G]So I made it to the outskirts of [D]town,
All my friends were waiting and they [A]made me praying
For the kinghood and a [G]crown.
Now I don't know what I want any[D]more.
In a blink of an eye, all the [A]parts went by,
So I waited for the [G]storm
To wake me like an al[A]arm.
{end_of_verse}

{start_of_chorus: Chorus 1}
There's an [D]angel on Earth, gonna [A]hit rock bottom.
An [Bm]angel on Earth, gonna [G]hit the sea.
And it won't [D]get any worse and it won't [A]get any better,
'Cause [Bm]after all, she's not like [G]you and me.
[D]Don't you know what's [C#m]coming when she [G]turns off [D]all the [C]lights?
There's an angel on Earth, gonna [A]hit rock bottom to[G]night. [A]
{end_of_chorus}

{start_of_bridge: Interlude}
[D] [A] [G] [A]
{end_of_bridge}

{start_of_verse: Verse 3}
[G]You know that time is all you need to push [D]through.
But it seems that the sun is [A]always on the run,
There's no minute over[G]due.
So you get up, dress up, take a deep [D]breath.
On the inside grows what [A]no one knows,
There's a strange pain in your [G]chest. [A]
{end_of_verse}

{start_of_chorus: Chorus 2}
There's an [D]angel on Earth, gonna [A]hit rock bottom.
An [Bm]angel on Earth, gonna [G]hit the sea.
And it won't [D]get any worse and it won't [A]get any better,
'Cause [Bm]after all, she's not like [G]you and me.
[D]Don't you know what's [C#m]coming when she [G]turns off [D]all the [C]lights?
{end_of_chorus}

{start_of_chorus: Chorus 3}
There's an [D]angel on Earth, gonna [A]hit rock bottom.
An [Bm]angel on Earth, gonna [G]hit the sea.
And it won't [D]get any worse and it won't [A]get any better,
'Cause [Bm]after all, she's not like [G]you and me.
[D]Don't you know what's [C#m]coming when she [G]turns off [D]all the [C]lights?
There's an angel on Earth, gonna [A]hit rock bottom to[G]night.
We're gonna [A]hit rock bottom to[Bm]night.
{end_of_chorus}

{start_of_bridge: Instrumental}
[Bm] [D] [G] [Bm]
[Bm] [A] [Em] [Em/F#] [G] [A]
{end_of_bridge}
