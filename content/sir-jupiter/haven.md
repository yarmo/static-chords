+++
title = "Haven"
+++

{title: Haven}
{artist: Sir Jupiter}
{year: 2013}

{start_of_bridge: Intro}
[Dm] [F] [Dm] [C]
[Dm] [F] [Dm] [C] [Dm]
[C] [Dm] [C] [Dm]
[F] [C] [Am] [Em] [G]
{end_of_bridge}

{start_of_verse: Verse 1}
[Dm]Under the [F]old tree, you'll find [Bb]heaven.
[Gm]Or at least, that's [C]what he said to [Dm]fool you. [Fsus4]
[Dm]Hear my pi[F]ano 'cross the [Bb]distance,
[Gm]Sweet tones to [C]guide and get you [Dm]through.
{end_of_verse}

{start_of_bridge: Interlude}
[D#] [F] [D#] [F] [D#] [D]
{end_of_bridge}

{start_of_verse: Verse 2}
[Dm]Feeling a [F]bit lost under [Bb]your skin,
[Gm]Wandering if [C]there's no better [Dm]way. [Fsus4]
[Dm]The winter [F]chill has got you [Bb]smiling,
[Gm]Turned your head a[C]round, you felt be[Dm]trayed.
{end_of_verse}

{start_of_chorus: Chorus 1}
[Bb]And this could be our [F]ha[C]ven.
[Bb]The smell and shine of our [F]ha[C]ven.
[Bb]Ooh-[C]ooh-[Dm]ooh [Gm] [F]
[Bb]And this could be our [F]ha[C]ven. [Dm]
[C] [Dm] [C] [Dm]
[F] [C] [Am] [Em] [G]
{end_of_chorus}

{start_of_bridge: Instrumental}
[F] [Am] [F] [C7]
[F] [Dm] [Am] [Bb] [C]
[F] [Dm] [Am] [C7]
[F] [Dm] [Am] [Bb] [C]
[Dm] [G] [Dm] [G]
[Dm] [C] [Dm] [C] [Bb] [A]
{end_of_bridge}

{start_of_verse: Verse 3}
[Dm]Clouds upon your [F]burning eyes start [Bb]melting.
[Gm]Peaceful [C]thoughts will join you [Dm]here.
[Gm] [C] [Dm]
{end_of_verse}

{start_of_chorus: Chorus 2}
[Bb]And this could be our [F]ha[C]ven.
[Bb]The smell and shine of our [F]ha[C]ven.
[Bb]Ooh-[C]ooh-[Dm]ooh [Gm] [F]
[Bb]And this could be our [F]ha[C]ven. [Dm]
[C] [Dm] [C] [Dm]
[F] [C] [Am] [Em] [G]
{end_of_chorus}

{start_of_bridge: Outro}
[Bb] [F] [C]
{end_of_bridge}
